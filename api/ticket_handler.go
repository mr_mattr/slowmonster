package api

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/server/keys"
	"gitlab.com/mr_mattr/slowmonster/server/tickets"
	"golang.org/x/net/context"
)

//GetTickets lists a user's tickets
func (s *Server) GetTickets(q *TicketQuery, stream TicketServ_GetTicketsServer) error {
	id, err := getUserIDFromContext(stream.Context())
	if err != nil {
		return err
	}

	t, err := tickets.GetTicketsByUserID(id)
	if err != nil {
		return err
	}

	for _, n := range t {
		stream.Send(ticketToA(&n))
	}

	return nil
}

//CreateTicket creates a new ticket
func (s *Server) CreateTicket(ctx context.Context, t *Ticket) (*Ticket, error) {
	userID, err := getUserIDFromContext(ctx)
	if err != nil {
		return &Ticket{}, err
	}

	newT, err := tickets.CreateTicket(userID, aToTicket(t))
	if err != nil {
		return &Ticket{}, err
	}

	return ticketToA(newT), nil
}

func aToTicket(a *Ticket) *tickets.Ticket {
	c := time.Unix(0, a.ClosedAt)
	return &tickets.Ticket{
		ID:          a.Id,
		Description: a.Description,
		ClosedAt:    &c,
		DaysInWeek:  float32(a.DaysInWeek),
		Priority:    a.Priority,
	}
}

func ticketToA(t *tickets.Ticket) *Ticket {
	return &Ticket{
		Id:          t.ID,
		Description: t.Description,
		ClosedAt:    t.ClosedAt.Unix(),
		DaysInWeek:  float64(t.DaysInWeek),
		Priority:    t.Priority,
	}
}

func getUserIDFromContext(ctx context.Context) (int64, error) {
	id := ctx.Value(keys.UserID{})
	if id == nil {
		return 0, errors.New("missing user id in request context")
	}

	idi, ok := id.(int64)
	if !ok {
		return 0, errors.Errorf("request user id is of wrong type: %[1]v is of type %[1]T", id)
	}

	return idi, nil
}
