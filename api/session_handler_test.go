package api_test

import (
	"context"
	"testing"

	"gitlab.com/mr_mattr/slowmonster/api"
)

func TestSessionCreateSuccess(t *testing.T) {
	s := api.Server{}

	l := &api.Login{}
	resp, err := s.Create(context.Background(), l)
	if err != nil {
		t.Fatal(err)
	}

	const token = "test token"
	if resp.Token != token {
		t.Errorf("unexpected token: %s is not %s", resp.Token, token)
	}
}
