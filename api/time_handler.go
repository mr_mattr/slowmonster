package api

import (
	"time"

	"gitlab.com/mr_mattr/slowmonster/server/tickets"
	"golang.org/x/net/context"
)

//GetTimes lists the recorded times according to a query
func (s *Server) GetTimes(q *TimeQuery, stream TimeServ_GetTimesServer) error {
	id, err := getUserIDFromContext(stream.Context())
	if err != nil {
		return err
	}

	tq := aToTimeQuery(q)

	t, err := tickets.GetTimesByUserID(id, tq)
	if err != nil {
		return err
	}

	for _, n := range t {
		stream.Send(timeToA(&n))
	}

	return nil
}

//CreateTime creates a new time against a ticket
func (s *Server) CreateTime(ctx context.Context, t *Time) (*Time, error) {
	userID, err := getUserIDFromContext(ctx)
	if err != nil {
		return &Time{}, err
	}

	newT, err := tickets.CreateTime(userID, aToTime(t))
	if err != nil {
		return &Time{}, err
	}

	return timeToA(newT), nil
}

//UpdateTime updates an existing time record
func (s *Server) UpdateTime(ctx context.Context, t *Time) (*Time, error) {
	userID, err := getUserIDFromContext(ctx)
	if err != nil {
		return &Time{}, err
	}

	updT, err := tickets.UpdateTime(userID, aToTime(t))
	if err != nil {
		return &Time{}, err
	}

	return timeToA(updT), nil
}

//DeleteTime updates an existing time record
func (s *Server) DeleteTime(ctx context.Context, t *Time) (*Time, error) {
	userID, err := getUserIDFromContext(ctx)
	if err != nil {
		return &Time{}, err
	}

	delT, err := tickets.DeleteTime(userID, aToTime(t))
	if err != nil {
		return &Time{}, err
	}

	return timeToA(delT), nil
}

func aToTime(a *Time) *tickets.Time {
	t := &tickets.Time{
		ID:          a.Id,
		TicketID:    a.TicketId,
		Description: a.Description,
	}

	if a.StartedAt > 0 {
		s := time.Unix(0, a.StartedAt)
		t.StartedAt = &s
	}

	if a.BrokeAt > 0 {
		b := time.Unix(0, a.BrokeAt)
		t.BrokeAt = &b
	}

	if a.EndedAt > 0 {
		e := time.Unix(0, a.EndedAt)
		t.EndedAt = &e
	}

	return t
}

func timeToA(t *tickets.Time) *Time {
	a := &Time{
		Id:          t.ID,
		TicketId:    t.TicketID,
		Description: t.Description,
		StartedAt:   0,
		BrokeAt:     0,
		EndedAt:     0,
	}

	if t.StartedAt != nil {
		a.StartedAt = t.StartedAt.UnixNano()
	}

	if t.BrokeAt != nil {
		a.BrokeAt = t.BrokeAt.UnixNano()
	}

	if t.EndedAt != nil {
		a.EndedAt = t.EndedAt.UnixNano()
	}

	return a
}

func aToTimeQuery(a *TimeQuery) *tickets.TimeQuery {
	t := &tickets.TimeQuery{
		OmitEnded: a.OmitEnded,
	}

	if a.StartedAfter > 0 {
		sa := time.Unix(0, a.StartedAfter)
		t.StartedAfter = &sa
	}

	if a.StartedBefore > 0 {
		sb := time.Unix(0, a.StartedBefore)
		t.StartedBefore = &sb
	}

	return t
}
