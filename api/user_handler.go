package api

import (
	"gitlab.com/mr_mattr/slowmonster/server/auth"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//CreateUser adds a new user to the system
func (s *Server) CreateUser(ctx context.Context, u *User) (*User, error) {
	//TODO validate existence of username and password
	//here or somewhere else?
	nu, err := auth.CreateUser(aToUser(u))
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to create user: %s", err)
	}

	return userToA(nu), nil
}

func userToA(u *auth.User) *User {
	return &User{
		Id:       u.ID,
		Username: u.Username,
	}
}

func aToUser(u *User) *auth.User {
	return &auth.User{
		ID:       u.Id,
		Username: u.Username,
		Password: u.Password,
	}
}
