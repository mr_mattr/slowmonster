package api

import (
	fmt "fmt"
	"time"

	"gitlab.com/mr_mattr/slowmonster/server/tickets"
	context "golang.org/x/net/context"
)

//GetAmounts calls the API to get all amounts based
//on query parameters
func (s *Server) GetAmounts(q *AmountQuery, stream AmountServ_GetAmountsServer) error {
	id, err := getUserIDFromContext(stream.Context())
	if err != nil {
		return err
	}

	aq := aToAmountQuery(q)

	a, err := tickets.GetAmountsByUserID(id, aq)
	if err != nil {
		return err
	}

	for _, n := range a {
		stream.Send(amountToA(&n))
	}

	return nil
}

//CreateAmount creates a new amount against a ticket
func (s *Server) CreateAmount(ctx context.Context, m *Amount) (*Amount, error) {
	userID, err := getUserIDFromContext(ctx)
	if err != nil {
		return &Amount{}, err
	}

	newT, err := tickets.CreateAmount(userID, aToAmount(m))
	if err != nil {
		return &Amount{}, err
	}

	return amountToA(newT), nil
}

//UpdateAmount updates an existing amount record
func (s *Server) UpdateAmount(ctx context.Context, m *Amount) (*Amount, error) {
	userID, err := getUserIDFromContext(ctx)
	if err != nil {
		return &Amount{}, err
	}
	fmt.Printf("###### at: %#v\n", m)

	updT, err := tickets.UpdateAmount(userID, aToAmount(m))
	if err != nil {
		return &Amount{}, err
	}

	return amountToA(updT), nil
}

func aToAmount(a *Amount) *tickets.Amount {
	m := &tickets.Amount{
		ID:          a.Id,
		TicketID:    a.TicketId,
		Description: a.Description,
		Amount:      a.Amount,
	}

	if a.AmountedAt > 0 {
		s := time.Unix(0, a.AmountedAt)
		m.AmountedAt = &s
	}

	return m
}

func amountToA(m *tickets.Amount) *Amount {
	a := &Amount{
		Id:          m.ID,
		TicketId:    m.TicketID,
		Description: m.Description,
		Amount:      m.Amount,
		AmountedAt:  0,
	}

	if m.AmountedAt != nil {
		a.AmountedAt = m.AmountedAt.UnixNano()
	}

	return a
}

func aToAmountQuery(a *AmountQuery) *tickets.AmountQuery {
	m := &tickets.AmountQuery{}

	if a.AmountedAfter > 0 {
		sa := time.Unix(0, a.AmountedAfter)
		m.AmountedAfter = &sa
	}

	if a.AmountedBefore > 0 {
		sb := time.Unix(0, a.AmountedBefore)
		m.AmountedBefore = &sb
	}

	return m
}
