package api

import (
	"context"

	"gitlab.com/mr_mattr/slowmonster/server/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//Create authenticates a user and returns a session token
func (s *Server) Create(ctx context.Context, l *Login) (*Session, error) {
	sess, err := auth.Login(l.Username, l.Password, s.PrivateKeyFile)
	if err != nil {
		return nil, grpc.Errorf(codes.PermissionDenied, err.Error())
	}

	return &Session{
		Token: sess.Token,
	}, nil
}
