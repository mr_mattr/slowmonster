package main

import (
	"context"
	"fmt"
	"io"
	"log"

	"gitlab.com/mr_mattr/slowmonster/api"
	"google.golang.org/grpc"
)

func main() {
	var conn *grpc.ClientConn

	conn, err := grpc.Dial(":7777", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := api.NewTicketServClient(conn)
	stream, err := c.GetTickets(context.Background(), &api.TicketQuery{UserId: 123})
	if err != nil {
		log.Fatalf("Error getting stream: %v", err)
	}
	for {
		ticket, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("failed receiving: %v", err)
		}

		fmt.Printf("Ticketz: %#v\n", ticket)
	}
}
