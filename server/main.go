package main

import (
	"fmt"
	"log"
	"net"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/server/auth"
	"gitlab.com/mr_mattr/slowmonster/server/database"
	"gitlab.com/mr_mattr/slowmonster/server/tickets"
	"google.golang.org/grpc"
)

func main() {
	c, err := dbConfig()
	if err != nil {
		log.Fatalf("failed to build database configuration: %+v", err)
	}

	if err := database.SetDB(c); err != nil {
		log.Fatalf("failed to set database connection: %+v", err)
	}

	if err := tickets.SetNamedStmts(); err != nil {
		log.Fatalf("failed to set named statements: %+v", err)
	}

	if err := serve(viper.GetInt("listen_port"), viper.GetString("private_key_file")); err != nil {
		log.Fatalf("failed to serve: %+v", err)
	}
}

func serve(port int, keyFile string) error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return errors.Wrapf(err, "error listening on port %d", port)
	}

	s := api.Server{
		PrivateKeyFile: keyFile,
	}

	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(auth.UnaryInterceptor),
		grpc.StreamInterceptor(auth.StreamInterceptor),
	)

	api.RegisterAmountServServer(grpcServer, &s)
	api.RegisterSessionServServer(grpcServer, &s)
	api.RegisterTicketServServer(grpcServer, &s)
	api.RegisterTimeServServer(grpcServer, &s)
	api.RegisterUserServServer(grpcServer, &s)

	fmt.Printf("starting GRPC server on port %d\n", port)

	if err := grpcServer.Serve(lis); err != nil {
		return errors.Wrap(err, "error starting GRPC server")
	}

	return nil
}

func dbConfig() (*database.Config, error) {
	if err := getConfig(); err != nil {
		return &database.Config{}, err
	}

	return &database.Config{
		Driver:   viper.GetString("db_driver"),
		User:     viper.GetString("db_user"),
		Password: viper.GetString("db_password"),
		DBName:   viper.GetString("db_dbname"),
		Host:     viper.GetString("db_host"),
		Port:     viper.GetInt("db_port"),
		SSLMode:  viper.GetString("db_sslmode"),
	}, nil
}

func getConfig() error {
	home, err := homedir.Dir()
	if err != nil {
		return errors.Wrap(err, "failed to get home dir")
	}

	viper.AddConfigPath(home)
	viper.AddConfigPath(".")
	viper.SetConfigName(".slowmosrv")
	viper.SetEnvPrefix("slowmosrv")
	viper.AutomaticEnv()

	//only use the config file if there was no error, otherwise, default to env vars
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	return nil
}
