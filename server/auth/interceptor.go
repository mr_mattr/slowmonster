package auth

import (
	"context"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/server/keys"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

//TODO pass this value from server
const publicKeyFile = "/root/.ssh/slowmo_id_ecdsa.pub"

var noAuth = []string{"/api.SessionServ/Create", "/api.UserServ/CreateUser"}

//UnaryInterceptor is the unary middleware that checks authentication
func UnaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	if isAuthRoute(info.FullMethod) {
		var err error
		ctx, err = authenticate(ctx)
		if err != nil {
			return nil, err
		}
	}

	return handler(ctx, req)
}

//StreamInterceptor is the stream middleware that checks authentication
func StreamInterceptor(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	//TODO how secure is this? is the info.FullMethod actually
	//used for routing, or can it be specified by the client?
	if isAuthRoute(info.FullMethod) {
		ctx := ss.Context()

		aCtx, err := authenticate(ctx)
		if err != nil {
			return err
		}

		ss = serverStreamWrapper{
			ServerStream: ss,
			ctx:          aCtx,
		}
	}

	return handler(srv, ss)
}

func authenticate(ctx context.Context) (context.Context, error) {
	meta, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return ctx, grpc.Errorf(codes.Unauthenticated, "missing context metadata")
	}

	if len(meta["token"]) != 1 {
		return ctx, grpc.Errorf(codes.Unauthenticated, "invalid token")
	}

	token, err := jwt.Parse(meta["token"][0], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
			return nil, errors.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		pemData, err := ioutil.ReadFile(publicKeyFile)
		if err != nil {
			return nil, errors.Wrap(err, "failed to read private key file")
		}

		blockPub, _ := pem.Decode(pemData)
		x509EncodedPub := blockPub.Bytes
		genericPublicKey, err := x509.ParsePKIXPublicKey(x509EncodedPub)
		if err != nil {
			return nil, errors.Wrap(err, "failed to parse the public key")
		}
		publicKey := genericPublicKey.(*ecdsa.PublicKey)

		return publicKey, nil
	})
	if err != nil {
		return ctx, errors.Wrap(err, "failed to parse token")
	}
	if !token.Valid {
		return ctx, errors.New("invalid token")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return ctx, errors.New("failed to process token")
	}

	idX, ok := claims["user_id"]
	if !ok {
		return ctx, errors.New("missing user ID in token")
	}

	idF, ok := idX.(float64)
	if !ok {
		return ctx, errors.Errorf("error parsing user ID as a float64: %v", idX)
	}

	return context.WithValue(ctx, keys.UserID{}, int64(idF)), nil
}

func isAuthRoute(r string) bool {
	for _, n := range noAuth {
		if r == n {
			return false
		}
	}

	return true
}

type serverStreamWrapper struct {
	grpc.ServerStream
	ctx context.Context
}

func (w serverStreamWrapper) Context() context.Context {
	return w.ctx
}
