package auth

import (
	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/server/database"
	"golang.org/x/crypto/bcrypt"
)

//User represents a single system user
type User struct {
	ID             int64  `db:"id" json:"id"`
	Username       string `db:"username" json:"username"`
	Password       string `json:"password"`
	HashedPassword []byte `db:"hashed_password"`
}

//GetUserByUsername returns a persisted user given a username
func GetUserByUsername(username string) (User, error) {
	u := User{}
	if err := database.DB.Get(&u, "SELECT * FROM users WHERE username = $1", username); err != nil {
		return User{}, errors.Wrap(err, "error fetching user by username")
	}

	return u, nil
}

//CreateUser adds a new user record to the database
//and populates the new ID
func CreateUser(u *User) (*User, error) {
	if err := u.hashPassword(); err != nil {
		return &User{}, err
	}

	stmt, err := database.DB.PrepareNamed("INSERT INTO users (username, hashed_password) VALUES (:username, :hashed_password) RETURNING id")
	if err != nil {
		return &User{}, errors.Wrap(err, "failed preparing user statement")
	}

	var id int64
	if err := stmt.Get(&id, u); err != nil {
		return &User{}, errors.Wrap(err, "failed creating new user record")
	}

	u.ID = id
	return u, nil
}

func (u *User) hashPassword() error {
	h, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return errors.Wrap(err, "failed to hash password")
	}
	u.HashedPassword = h

	return nil
}
