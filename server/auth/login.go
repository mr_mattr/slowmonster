package auth

import (
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//Session contains a session token
type Session struct {
	Token string
}

//Login takes a username and password, authenticates the user,
//and returns a session object
func Login(username, password, privateKeyFile string) (Session, error) {
	user, err := GetUserByUsername(username)
	if err != nil {
		return Session{}, err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.HashedPassword), []byte(password)); err != nil {
		return Session{}, errors.Wrap(err, "username or password is incorrect")
	}

	token := jwt.NewWithClaims(
		jwt.SigningMethodES512,
		jwt.MapClaims{
			"exp":     time.Now().Add(time.Hour * 24).Unix(),
			"iat":     time.Now().Unix(),
			"user_id": user.ID,
		},
	)

	certData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		err := errors.Wrap(err, "failed to read private key file")
		return Session{}, grpc.Errorf(codes.Internal, err.Error())
	}

	block, _ := pem.Decode(certData)
	if block == nil {
		return Session{}, errors.New("failed to decode key data")
	}
	if block.Type != "PRIVATE KEY" {
		return Session{}, errors.New("key is of the wrong type")
	}

	key, err := x509.ParseECPrivateKey(block.Bytes)
	if err != nil {
		err := errors.Wrap(err, "failed to parse private key file")
		return Session{}, grpc.Errorf(codes.Internal, err.Error())
	}

	tokenString, err := token.SignedString(key)
	if err != nil {
		err := errors.Wrap(err, "error signing JWT")
		return Session{}, grpc.Errorf(codes.Internal, err.Error())
	}

	return Session{tokenString}, nil
}
