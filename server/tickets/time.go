package tickets

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/server/database"
)

//Time represents time logged against a ticket
type Time struct {
	ID          int64      `db:"id" json:"id"`
	UserID      int64      `db:"user_id" json:"user_id"`
	TicketID    int64      `db:"ticket_id" json:"ticket_id"`
	Description string     `db:"description" json:"description"`
	StartedAt   *time.Time `db:"started_at" json:"started_at"`
	BrokeAt     *time.Time `db:"broke_at" json:"broke_at"`
	EndedAt     *time.Time `db:"ended_at" json:"ended_at"`
}

//TimeQuery defines the parameters for listing times
type TimeQuery struct {
	UserID        int64      `db:"user_id"`
	StartedAfter  *time.Time `db:"started_after"`
	StartedBefore *time.Time `db:"started_before"`
	OmitEnded     bool
}

//GetTimesByUserID returns all times in the database associated with a user id
func GetTimesByUserID(userID int64, tq *TimeQuery) ([]Time, error) {
	tq.UserID = userID

	q := []string{"SELECT times.id, times.user_id, ticket_id, description, started_at, broke_at, ended_at FROM times LEFT OUTER JOIN tickets ON times.ticket_id = tickets.id where times.user_id = :user_id"}

	if tq.StartedAfter != nil {
		q = append(q, "started_at >= :started_after")
	}

	if tq.StartedBefore != nil {
		q = append(q, "started_at < :started_before")
	}

	if tq.OmitEnded {
		q = append(q, "ended_at IS NULL")
	}

	s := strings.Join(q, " AND ")
	stmt, err := database.DB.PrepareNamed(s)
	if err != nil {
		return []Time{}, errors.Wrap(err, "failed to create the time lister named statement")
	}

	t := []Time{}
	if err := stmt.Select(&t, tq); err != nil {
		return []Time{}, errors.Wrap(err, "error fetching times by user id")
	}

	return t, nil
}

//CreateTime adds a new time record to the database
func CreateTime(userID int64, t *Time) (*Time, error) {
	t.UserID = userID

	if err := timeInserter.Get(&t.ID, t); err != nil {
		return &Time{}, errors.Wrap(err, "failed to insert new time record")
	}

	return t, nil
}

//UpdateTime updates a time record in the database
func UpdateTime(userID int64, t *Time) (*Time, error) {
	//ensures users can only find and return values from their own times
	t.UserID = userID

	var oldT Time
	if err := timeFinder.Get(&oldT, t); err != nil {
		return &Time{}, errors.Wrap(err, "failed to find time record to update")
	}

	//it would be more efficient to update the time in one query by building up the
	//sql query string, but this seems more straightforward, returning the full updated
	//time object
	newT := updateTime(&oldT, t)

	//ensures users can only update their own times
	newT.UserID = userID

	res, err := timeUpdater.Exec(newT)
	if err != nil {
		return &Time{}, errors.Wrap(err, "failed to update time record")
	}

	//TODO check the result for something?
	fmt.Printf("###### update result: %#v\n", res)

	return newT, nil
}

//DeleteTime deletes a time record from the database
func DeleteTime(userID int64, t *Time) (*Time, error) {
	//ensures users can only find and return values from their own times
	t.UserID = userID

	res, err := timeDeleter.Exec(t)
	if err != nil {
		return &Time{}, errors.Wrap(err, "failed to delete time record")
	}

	//TODO handle the result
	fmt.Printf("###### delete results: %#v\n", res)

	return t, nil
}

func updateTime(o, n *Time) *Time {
	if n.StartedAt != nil {
		o.StartedAt = n.StartedAt
	}

	if n.BrokeAt != nil {
		o.BrokeAt = n.BrokeAt
	}

	if n.EndedAt != nil {
		o.EndedAt = n.EndedAt
	}

	return o
}
