package tickets

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/server/database"
)

//Amount represents an amount logged against a ticket
type Amount struct {
	ID          int64      `db:"id" json:"id"`
	UserID      int64      `db:"user_id" json:"user_id"`
	TicketID    int64      `db:"ticket_id" json:"ticket_id"`
	Description string     `db:"description" json:"description"`
	Amount      float64    `db:"amount" json:"amount"`
	AmountedAt  *time.Time `db:"amounted_at" json:"amounted_at"`
}

//AmountQuery defines the parameters for listing times
type AmountQuery struct {
	UserID         int64      `db:"user_id"`
	AmountedAfter  *time.Time `db:"amounted_after"`
	AmountedBefore *time.Time `db:"amounted_before"`
}

//GetAmountsByUserID returns all amounts in the database associated with a user id
func GetAmountsByUserID(userID int64, aq *AmountQuery) ([]Amount, error) {
	aq.UserID = userID

	q := []string{"SELECT amounts.id, amounts.user_id, ticket_id, description, amount, amounted_at FROM amounts LEFT OUTER JOIN tickets ON amounts.ticket_id = tickets.id where amounts.user_id = :user_id"}

	if aq.AmountedAfter != nil {
		q = append(q, "amounted_at >= :amounted_after")
	}

	if aq.AmountedBefore != nil {
		q = append(q, "amounted_at < :amounted_before")
	}

	s := strings.Join(q, " AND ")
	stmt, err := database.DB.PrepareNamed(s)
	if err != nil {
		return []Amount{}, errors.Wrap(err, "failed to create the amount lister named statement")
	}

	a := []Amount{}
	if err := stmt.Select(&a, aq); err != nil {
		return []Amount{}, errors.Wrap(err, "error fetching amounts by user id")
	}

	return a, nil
}

//CreateAmount adds a new amount record to the database
func CreateAmount(userID int64, a *Amount) (*Amount, error) {
	a.UserID = userID

	if err := amountInserter.Get(&a.ID, a); err != nil {
		return &Amount{}, errors.Wrap(err, "failed to insert new amount record")
	}

	return a, nil
}

//UpdateAmount updates a amount record in the database
func UpdateAmount(userID int64, m *Amount) (*Amount, error) {
	//ensures users can only find and return values from their own amounts
	m.UserID = userID

	var oldM Amount
	if err := amountFinder.Get(&oldM, m); err != nil {
		return &Amount{}, errors.Wrap(err, "failed to find amount record to update")
	}

	//it would be more efficient to update the amount in one query by building up the
	//sql query string, but this seems more straightforward, returning the full updated
	//amount object
	newM := updateAmount(&oldM, m)

	//ensures users can only update their own amounts
	newM.UserID = userID

	res, err := amountUpdater.Exec(newM)
	if err != nil {
		return &Amount{}, errors.Wrap(err, "failed to update amount record")
	}

	//TODO check the result for something?
	fmt.Printf("###### update result: %#v\n", res)

	return newM, nil
}

func updateAmount(o, n *Amount) *Amount {
	if n.AmountedAt != nil {
		o.AmountedAt = n.AmountedAt
	}

	//TODO should a user be able to update an amount record without updating the amount?
	o.Amount = n.Amount

	return o
}
