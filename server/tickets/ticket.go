package tickets

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/server/database"
)

//Ticket is an activity that a user can log time or amounts against
type Ticket struct {
	ID          int64      `db:"id" json:"id"`
	UserID      int64      `db:"user_id" json:"user_id"`
	Description string     `db:"description" json:"description"`
	ClosedAt    *time.Time `db:"closed_at" json:"closed_at"`
	DaysInWeek  float32    `db:"days_in_week" json:"days_in_week"`
	Priority    int64      `db:"priority" json:"priority"`
}

//GetTicketsByUserID returns all tickets in the database associated with a user id
func GetTicketsByUserID(id int64) ([]Ticket, error) {
	t := []Ticket{}
	if err := database.DB.Select(&t, "SELECT * FROM tickets where user_id = $1", id); err != nil {
		return []Ticket{}, errors.Wrap(err, "error fetching tickets by user id")
	}

	return t, nil
}

//CreateTicket adds a ticket record to the database
func CreateTicket(userID int64, t *Ticket) (*Ticket, error) {
	t.UserID = userID

	if err := ticketInserter.Get(&t.ID, t); err != nil {
		return &Ticket{}, errors.Wrap(err, "failed to insert new ticket record")
	}

	return t, nil
}
