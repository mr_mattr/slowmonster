package tickets

import (
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/server/database"
)

var (
	amountInserter *sqlx.NamedStmt
	amountFinder   *sqlx.NamedStmt
	amountUpdater  *sqlx.NamedStmt
	ticketInserter *sqlx.NamedStmt
	timeInserter   *sqlx.NamedStmt
	timeFinder     *sqlx.NamedStmt
	timeUpdater    *sqlx.NamedStmt
	timeDeleter    *sqlx.NamedStmt
)

//SetNamedStmts initializes named statements
func SetNamedStmts() error {
	var err error

	amountInserter, err = database.DB.PrepareNamed("INSERT INTO amounts (user_id, ticket_id, amount, amounted_at) VALUES (:user_id, :ticket_id, :amount, :amounted_at) RETURNING id")
	if err != nil {
		return errors.Wrap(err, "failed to create amount inserter named statement")
	}

	amountFinder, err = database.DB.PrepareNamed("SELECT * FROM amounts WHERE id = :id and user_id = :user_id")
	if err != nil {
		return errors.Wrap(err, "failed to create amount finder named statement")
	}

	amountUpdater, err = database.DB.PrepareNamed("UPDATE amounts SET amount = :amount, amounted_at = :amounted_at WHERE user_id = :user_id AND id = :id")
	if err != nil {
		return errors.Wrap(err, "failed to create amount updater named statement")
	}

	ticketInserter, err = database.DB.PrepareNamed("INSERT INTO tickets (user_id, description, closed_at, days_in_week, priority) VALUES (:user_id, :description, :closed_at, :days_in_week, :priority) RETURNING id")
	if err != nil {
		return errors.Wrap(err, "failed to create ticket inserter named statement")
	}

	timeInserter, err = database.DB.PrepareNamed("INSERT INTO times (user_id, ticket_id, started_at, broke_at, ended_at) VALUES (:user_id, :ticket_id, :started_at, :broke_at, :ended_at) RETURNING id")
	if err != nil {
		return errors.Wrap(err, "failed to create time inserter named statement")
	}

	timeFinder, err = database.DB.PrepareNamed("SELECT * FROM times WHERE id = :id and user_id = :user_id")
	if err != nil {
		return errors.Wrap(err, "failed to create time finder named statement")
	}

	timeUpdater, err = database.DB.PrepareNamed("UPDATE times SET started_at = :started_at, broke_at = :broke_at, ended_at = :ended_at WHERE user_id = :user_id AND id = :id")
	if err != nil {
		return errors.Wrap(err, "failed to create time updater named statement")
	}

	timeDeleter, err = database.DB.PrepareNamed("DELETE FROM times WHERE user_id = :user_id AND id = :id")
	if err != nil {
		return errors.Wrap(err, "failed to create time deleter named statement")
	}

	return nil
}
