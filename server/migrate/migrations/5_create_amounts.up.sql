CREATE TABLE amounts (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES users(id),
    ticket_id INTEGER NOT NULL REFERENCES tickets(id),
    amount DECIMAL NOT NULL,
    amounted_at TIMESTAMP NOT NULL
)
