CREATE TABLE tickets (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES users(id),
    description TEXT NOT NULL,
    closed_at TIMESTAMP,
    days_in_week DECIMAL(10, 2),
    priority INTEGER
)

