CREATE TABLE times (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES users(id),
    ticket_id INTEGER NOT NULL REFERENCES tickets(id),
    description TEXT,
    started_at TIMESTAMP NOT NULL,
    broke_at TIMESTAMP,
    ended_at TIMESTAMP
)
