package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/mr_mattr/slowmonster/server/database"
)

var dir = "migrate/migrations"

func main() {
	config, err := dbConfig()
	if err != nil {
		log.Fatalf("failed getting db config: %+v", err)
	}

	if err := database.SetDB(config); err != nil {
		log.Fatalf("failed connecting to db: %+v", err)
	}

	driver, err := postgres.WithInstance(database.DB.DB, &postgres.Config{})
	if err != nil {
		log.Fatalf("failed creating db driver: %+v", err)
	}

	wd, err := os.Getwd()
	if err != nil {
		log.Fatalf("failed finding the current working directory: %+v", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		filepath.Join("file://", wd, "migrate", "migrations"),
		//filepath.Join("file://", "root", "code", "src", "github.com", "composit", "slowmo", "migrate", "migrations"),
		"postgres", driver)
	if err != nil {
		log.Fatalf("failed creating db instance: %+v", err)
	}

	if err := m.Migrate(5); err != nil {
		log.Fatalf("failed migrating: %+v", err)
	}
}

func dbConfig() (*database.Config, error) {
	if err := getConfig(); err != nil {
		return &database.Config{}, err
	}

	return &database.Config{
		Driver:   viper.GetString("db_driver"),
		User:     viper.GetString("db_user"),
		Password: viper.GetString("db_password"),
		DBName:   viper.GetString("db_dbname"),
		Host:     viper.GetString("db_host"),
		Port:     viper.GetInt("db_port"),
		SSLMode:  viper.GetString("db_sslmode"),
	}, nil
}

func getConfig() error {
	home, err := homedir.Dir()
	if err != nil {
		return errors.Wrap(err, "failed to get home dir")
	}

	viper.AddConfigPath(home)
	viper.AddConfigPath(".")
	viper.SetConfigName(".slowmosrv")
	viper.SetEnvPrefix("slowmosrv")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	return nil
}
