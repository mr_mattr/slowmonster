package database

import (
	"fmt"
	"strings"
)

//Config contains values used to connect to the database
type Config struct {
	Driver   string
	User     string
	Password string
	DBName   string
	Host     string
	Port     int
	SSLMode  string
}

func (c *Config) connectStr() string {
	s := []string{}

	if u := c.User; u != "" {
		s = append(s, fmt.Sprintf("user=%s", u))
	}

	if p := c.Password; p != "" {
		s = append(s, fmt.Sprintf("password=%s", p))
	}

	if n := c.DBName; n != "" {
		s = append(s, fmt.Sprintf("dbname=%s", n))
	}

	if h := c.Host; h != "" {
		s = append(s, fmt.Sprintf("host=%s", h))
	}

	if p := c.Port; p != 0 {
		s = append(s, fmt.Sprintf("port=%s", portString(p)))
	}

	if m := c.SSLMode; m != "" {
		s = append(s, fmt.Sprintf("sslmode=%s", m))
	}

	return strings.Join(s, " ")
}

func portString(p int) string {
	if p == 0 {
		return ""
	}

	return fmt.Sprintf("%d", p)
}
