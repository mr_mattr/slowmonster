package database

import (
	"github.com/jmoiron/sqlx"
	//driver for sqlx
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

//DB is a global variable holding the database connection
var DB *sqlx.DB

//SetDB connects to the database and sets the connection as a global var
func SetDB(c *Config) error {
	var err error
	DB, err = sqlx.Connect(c.Driver, c.connectStr())
	if err != nil {
		return errors.Wrap(err, "failed connecting to the database")
	}

	return nil
}
