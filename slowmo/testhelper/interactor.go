package testhelper

import (
	"bytes"
	"fmt"
	"time"

	"github.com/pkg/errors"
)

//Returner implements interactor and allows a test to determine the inputs and outputs of an edit
type Returner struct {
	editIn   []byte
	editOut  []byte
	printOut *bytes.Buffer
	readStr  string
	readPwd  string
	loc      *time.Location
}

//NewReturner returns an instance of Returner with input and output
func NewReturner() *Returner {
	return &Returner{
		printOut: bytes.NewBuffer([]byte{}),
		loc:      time.Local,
	}
}

//SetEdits sets the editIn and editOut values on the returner
func (r *Returner) SetEdits(i, o []byte) {
	r.editIn = i
	r.editOut = o
}

//SetReads sets the readStr and readPwd values on the returner
func (r *Returner) SetReads(s, p string) {
	r.readStr = s
	r.readPwd = p
}

//SetLoc sets the loc value on the returner
func (r *Returner) SetLoc(l *time.Location) {
	r.loc = l
}

//Wait implements interactor, but does nothing
func (r *Returner) Wait() error {
	return nil
}

//Edit checks that input matches expected and returns output
func (r *Returner) Edit(og []byte) ([]byte, error) {
	if string(og) != string(r.editIn) {
		return []byte{}, errors.Errorf("unexpected input\ngot:      '%s'\nexpected: '%s'", og, r.editIn)
	}

	return r.editOut, nil
}

//Display adds a string to the printOut buffer
func (r *Returner) Display(a ...interface{}) {
	fmt.Print()
	if _, err := fmt.Fprint(r.printOut, a...); err != nil {
		panic(err)
	}
}

//Displayf adds a formatted string to the printOut buffer
func (r *Returner) Displayf(c string, a ...interface{}) {
	if _, err := fmt.Fprintf(r.printOut, c, a...); err != nil {
		panic(err)
	}
}

//Displayln adds a string plus newline to the printOut buffer
func (r *Returner) Displayln(a ...interface{}) {
	if _, err := fmt.Fprintln(r.printOut, a...); err != nil {
		panic(err)
	}
}

//Read returns the stored readStr
func (r *Returner) Read() (string, error) {
	return r.readStr, nil
}

//ReadPassword returns the stored readPwd
func (r *Returner) ReadPassword() (string, error) {
	return r.readPwd, nil
}

//SetLocation sets the location
func (r *Returner) SetLocation(l *time.Location) {
	r.loc = l
}

//Location returns the stored loc
func (r *Returner) Location() *time.Location {
	return r.loc
}
