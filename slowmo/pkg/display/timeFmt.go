package display

import "time"

//EditFormat is the format a time should appear in the edit document
const EditFormat = "2006-01-02 15:04:05"

//TimeStr returns a time formatted for human readability in a specific location
func TimeStr(t *time.Time, loc *time.Location) string {
	lt := t.In(loc)
	if lt.YearDay() == time.Now().In(loc).YearDay() {
		return t.In(loc).Format("03:04:05 PM")
	}
	return lt.Format("Mon, Jan 2, 2006, at 03:04:05 PM")
}

//EditTimeStr returns a time formatted for editing in a specific location
func EditTimeStr(t *time.Time, loc *time.Location) string {
	if t == nil {
		return "                   "
	}
	lt := t.In(loc)
	return lt.Format(EditFormat)
}
