package display

import (
	"fmt"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
)

//BodyErrs returns an error returned in the body of an API request
func BodyErrs(body io.ReadCloser, event string) error {
	defer body.Close()

	rBody, err := ioutil.ReadAll(body)
	if err != nil {
		return errors.Wrap(err, "failed reading error response from the server")
	}

	return fmt.Errorf("there was an error %s: %s", event, string(rBody))
}
