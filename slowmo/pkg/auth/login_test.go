package auth_test

import (
	"fmt"
	"io/ioutil"
	"testing"

	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/auth"
	"gitlab.com/mr_mattr/slowmonster/slowmo/testhelper"
)

const testTokenFile = "/tmp/.test-slowmo-token"

func TestLoginSuccessfully(t *testing.T) {
	token := "some-old-token"
	c := &TestSessionClient{
		Session: &api.Session{
			Token: token,
		},
		TokenFile: testTokenFile,
	}

	u := testhelper.NewReturner()

	if err := auth.Login(c, u, "this", "that"); err != nil {
		t.Error(err)
	}

	tb, err := ioutil.ReadFile(c.TokenFile)
	if err != nil {
		t.Error(err)
	}

	if tbs, expected := string(tb), token; tbs != expected {
		t.Errorf("unexpected token: '%s' is not equal to '%s'", tbs, expected)
	}
}

func TestBadUsername(t *testing.T) {
	c := &TestSessionClient{
		Error: fmt.Errorf("bad login"),
	}

	u := testhelper.NewReturner()

	if err := auth.Login(c, u, "incorrect", "one"); err == nil {
		t.Error("bad login should produce an error")
	}
}

type TestSessionClient struct {
	Session   *api.Session
	Error     error
	TokenFile string
}

func (s *TestSessionClient) Login(l *api.Login) (*api.Session, error) {
	return s.Session, s.Error
}

func (s *TestSessionClient) GetTokenFile() string {
	return s.TokenFile
}
