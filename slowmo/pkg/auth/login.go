package auth

import (
	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//Login calls the API to log a user in and writes the token to the tokenfile
func Login(c client.SessionClient, u ui.Interactor, name, pwd string) error {
	if name == "" {
		var err error
		name, err = getName(u)
		if err != nil {
			return errors.Wrap(err, "failed to get username")
		}
	}

	if pwd == "" {
		var err error
		pwd, err = getPwd(u)
		if err != nil {
			return errors.Wrap(err, "failed to get password")
		}
	}

	q := api.Login{
		Username: name,
		Password: pwd,
	}
	resp, err := c.Login(&q)
	if err != nil {
		return errors.Wrap(err, "failed to authenticate with the API")
	}

	u.Displayf("Logged in as %s\n", name)

	if err := writeToken(c.GetTokenFile(), resp.Token); err != nil {
		return errors.Wrap(err, "failed to write token")
	}

	return nil
}

func getName(u ui.Interactor) (string, error) {
	u.Display("Username, please: ")

	return u.Read()
}

func getPwd(u ui.Interactor) (string, error) {
	u.Display("Password, please: ")

	return u.ReadPassword()
}
