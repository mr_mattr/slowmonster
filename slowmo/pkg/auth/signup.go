package auth

import (
	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//Signup calls the API to create a new user record
func Signup(c client.UserClient, u ui.Interactor, name, pwd string) error {
	if name == "" {
		var err error
		name, err = getName(u)
		if err != nil {
			return errors.Wrap(err, "failed to get username")
		}
	}

	if pwd == "" {
		var err error
		pwd, err = getPwd(u)
		if err != nil {
			return errors.Wrap(err, "failed to get password")
		}
	}

	q := api.User{
		Username: name,
		Password: pwd,
	}
	_, err := c.Signup(&q)
	if err != nil {
		return errors.Wrap(err, "failed to create the new user record")
	}

	u.Displayf("Created user %s. You can now log in as your new user.\n", name)

	return nil
}
