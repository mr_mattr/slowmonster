package auth

import (
	"io/ioutil"
)

func writeToken(f string, token string) error {
	return ioutil.WriteFile(f, []byte(token), 0600)
}

func readToken(f string) (string, error) {
	tb, err := ioutil.ReadFile(f)
	return string(tb), err
}
