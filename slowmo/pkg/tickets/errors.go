package tickets

import (
	"bytes"
	"fmt"
)

//LineErr represents an error parsing a line in the time edit file
type LineErr struct {
	Line  string
	Error error
}

//LineErrs holds a slice of LineErrs
type LineErrs struct {
	LineErrors []LineErr
}

//Error implements the error interface
func (l *LineErrs) Error() string {
	b := bytes.NewBuffer([]byte("There"))

	if e := len(l.LineErrors); e == 1 {
		b.WriteString(" was one error")
	} else {
		b.WriteString(fmt.Sprintf(" were %d errors", e))
	}

	b.WriteString(" processing those times")
	b.WriteRune('\n')
	b.WriteRune('\n')
	for _, e := range l.LineErrors {
		//err is always nil
		b.WriteString(e.Error.Error())
		b.WriteRune('\n')
		b.WriteString(e.Line)
		b.WriteRune('\n')
		b.WriteRune('\n')
	}

	return b.String()
}

//AppendErr appends a LineErr to LineErrs
func (l *LineErrs) AppendErr(line string, err error) {
	l.LineErrors = append(l.LineErrors, LineErr{

		Line:  line,
		Error: err,
	})
}

//Err returns nil if the LineErrs does not have a LineErr,
//otherwise it returns itself
func (l *LineErrs) Err() error {
	if len(l.LineErrors) == 0 {
		return nil
	}

	return l
}

type editErrs struct {
	Errors []error
}

func (e *editErrs) Error() string {
	b := bytes.NewBuffer([]byte{})
	for _, e := range e.Errors {
		b.WriteString(e.Error())
		b.WriteRune('\n')
	}

	return b.String()
}
