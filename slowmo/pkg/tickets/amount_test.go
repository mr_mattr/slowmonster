package tickets_test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
	"gitlab.com/mr_mattr/slowmonster/slowmo/testhelper"
)

func TestAddAmount(t *testing.T) {
	s := &testAmountClient{}
	u := testhelper.NewReturner()

	if err := tickets.Add(s, u, 123, 456.7, time.Now().UTC()); err != nil {
		t.Error(err)
	}
}

func TestAddBadAmount(t *testing.T) {
	s := &testAmountClient{
		Error: fmt.Errorf("test error"),
	}
	u := testhelper.NewReturner()

	// a ticket id that the user does not have access to
	err := tickets.Add(s, u, 456, 456.7, time.Now().UTC())
	if err == nil {
		t.Error("should have failed to add to the ticket")
	}

	if !strings.Contains(err.Error(), "test error") {
		t.Errorf("bad error message: %s", err)
	}
}

type testAmountClient struct {
	Amounts []api.Amount
	Error   error
}

func (c *testAmountClient) GetAmounts(q api.AmountQuery) ([]api.Amount, error) {
	return c.Amounts, c.Error
}

func (c *testAmountClient) CreateAmount(a api.Amount) error {
	return c.Error
}

func (c *testAmountClient) UpdateAmount(a api.Amount) error {
	return c.Error
}

func (c *testAmountClient) DeleteAmount(a api.Amount) error {
	return c.Error
}
