package tickets

import (
	"bufio"
	"bytes"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/display"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//EditAmounts opens an editor with the amounts returned by user-entered
//query parameters and allows the user to edit the amounts
func EditAmounts(c client.AmountClient, u ui.Interactor, tIDs []int64, amountedAfter time.Time, amountedBefore time.Time) error {
	q := api.AmountQuery{
		TicketIds:      tIDs,
		AmountedAfter:  amountedAfter.UnixNano(),
		AmountedBefore: amountedBefore.UnixNano(),
	}

	r, err := c.GetAmounts(q)
	if err != nil {
		return errors.Wrap(err, "error getting amounts to edit")
	}

	amounts := []Amount{}
	for _, a := range r {
		amounts = append(amounts, aToAmount(a))
	}

	b, err := byteifyAmounts(amounts, u.Location())
	if err != nil {
		return errors.Wrap(err, "error converting amounts to bytes")
	}

	n, err := u.Edit(b)
	if err != nil {
		return errors.Wrap(err, "error editing amounts")
	}

	newAmounts, err := debyteifyAmounts(n, u.Location())
	if err != nil {
		return errors.Wrap(err, "error converting bytes to amounts")
	}

	diff := diffAmounts(amounts, newAmounts)
	displayAmountDiff(u, diff)
	if err := u.Wait(); err != nil {
		return errors.Wrap(err, "error getting user input on diff")
	}

	return submitAmountUpdates(c, diff)
}

func submitAmountUpdates(c client.AmountClient, d amountDiff) error {
	//TODO combine all this into one call
	wg := &sync.WaitGroup{}
	ec := make(chan error)

	for _, a := range d.Creates {
		wg.Add(1)

		go func(a Amount) {
			if err := c.CreateAmount(amountToA(a)); err != nil {
				ec <- errors.Wrapf(err, "failed creating amount of %g for ticket %d at %v", a.Amount, a.TicketID, a.AmountedAt)
			}

			wg.Done()
		}(a)
	}

	for _, a := range d.Updates {
		wg.Add(1)

		go func(a Amount) {
			if err := c.UpdateAmount(amountToA(a)); err != nil {
				ec <- errors.Wrapf(err, "failed updating amount %d", a.ID)
			}
			wg.Done()
		}(a)
	}

	for _, a := range d.Deletes {
		wg.Add(1)

		go func(a Amount) {
			if err := c.DeleteAmount(amountToA(a)); err != nil {
				ec <- errors.Wrapf(err, "failed deleting amount %d", a.ID)
			}
			wg.Done()
		}(a)
	}

	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
		close(ec)
	}()

	errs := &editErrs{}
	for {
		select {
		case err := <-ec:
			errs.Errors = append(errs.Errors, err)
		case <-done:
			if len(errs.Errors) > 0 {
				return errs
			}
			return nil
		}
	}
}

func byteifyAmounts(amounts []Amount, loc *time.Location) ([]byte, error) {
	sort.Slice(amounts, func(i, j int) bool {
		return amounts[i].AmountedAt.Before(amounts[j].AmountedAt)
	})

	b := bytes.NewBuffer([]byte{})
	for _, a := range amounts {
		//TODO write bytes
		b.WriteString(fmt.Sprintf(
			"%d | %d | %s | %f | %s |\n",
			a.ID,
			a.TicketID,
			a.Description,
			a.Amount,
			display.EditTimeStr(&a.AmountedAt, loc),
		))
	}

	return b.Bytes(), nil
}

func debyteifyAmounts(raw []byte, loc *time.Location) ([]Amount, error) {
	newAmounts := []Amount{}
	le := &LineErrs{}
	scanner := bufio.NewScanner(bytes.NewReader(raw))
	for scanner.Scan() {
		amount, err := lineToAmount(scanner.Text(), loc)
		if err != nil {
			le.AppendErr(scanner.Text(), err)
			continue
		}

		newAmounts = append(newAmounts, amount)
	}

	if err := scanner.Err(); err != nil {
		return []Amount{}, errors.Wrap(err, "error scanning edit file")
	}

	if err := le.Err(); err != nil {
		return []Amount{}, errors.Wrap(err, "error parsing edit file")
	}

	return newAmounts, nil
}

const (
	_idIndex = iota
	_        //ticketIDIndex
	_        //descriptionIndex
	amountIndex
	amountedAtIndex
)

func lineToAmount(line string, loc *time.Location) (Amount, error) {
	// return on the first error. One error could cascade into many, with one cause
	parts := strings.Split(line, "|")

	a := Amount{Description: parts[descriptionIndex]}
	var err error

	//if the id is blank, it's a new record
	if id := strings.TrimSpace(parts[idIndex]); id != "" {
		a.ID, err = strconv.ParseInt(id, 10, 64)
		if err != nil {
			return Amount{}, errors.Wrap(err, "error parsing ID")
		}
	}

	if a.TicketID, err = strconv.ParseInt(strings.TrimSpace(parts[ticketIDIndex]), 10, 64); err != nil {
		return Amount{}, errors.Wrap(err, "error parsing ticket ID")
	}

	if a.Amount, err = strconv.ParseFloat(strings.TrimSpace(parts[amountIndex]), 64); err != nil {
		return Amount{}, errors.Wrap(err, "error parsing amount")
	}

	aa, err := getTime(parts[amountedAtIndex], loc)
	if err != nil {
		return Amount{}, errors.Wrap(err, "error parsing start time")
	}
	a.AmountedAt = *aa

	return a, nil
}

type amountDiff struct {
	Creates []Amount
	Updates []Amount
	Deletes []Amount
}

func diffAmounts(oa, na []Amount) amountDiff {
	ad := amountDiff{
		Deletes: findAmountDeletes(oa, na),
	}

OUTER:
	for _, n := range na {
		for _, o := range oa {
			if n.ID == o.ID {
				if amountUpdated(n, o) {
					ad.Updates = append(ad.Updates, n)
				}

				continue OUTER
			}
		}
		ad.Creates = append(ad.Creates, n)
	}

	return ad
}

//amountUpdated compares only the fields that a user is allowed to update
func amountUpdated(t1, t2 Amount) bool {
	same := t1.Amount == t2.Amount &&
		compareTimes(&t1.AmountedAt, &t2.AmountedAt)

	return !same
}

func findAmountDeletes(oa, na []Amount) []Amount {
	d := []Amount{}
OUTER:
	for _, o := range oa {
		for _, n := range na {
			if o.ID == n.ID {
				continue OUTER
			}
		}
		d = append(d, o)
	}

	return d
}

func displayAmountDiff(u ui.Interactor, ad amountDiff) {
	if len(ad.Creates)+len(ad.Updates)+len(ad.Deletes) == 0 {
		u.Displayln("No changes made")
		return
	}

	displayAmountLine(u, ad.Creates, "create")
	displayAmountLine(u, ad.Updates, "update")
	displayAmountLine(u, ad.Deletes, "delete")
}

func displayAmountLine(u ui.Interactor, a []Amount, c string) {
	if l := len(a); l > 0 {
		u.Displayf("This operation will %s %d amount", c, l)
		if l > 1 {
			u.Displayf("s")
		}
		u.Displayln("")
	}
}
