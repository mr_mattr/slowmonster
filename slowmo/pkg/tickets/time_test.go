package tickets_test

import (
	"encoding/json"
	"strings"
	"testing"
	"time"

	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
	"gitlab.com/mr_mattr/slowmonster/slowmo/testhelper"
)

func TestStartTime(t *testing.T) {
	c := &testTimeClient{}
	u := testhelper.NewReturner()

	tm := time.Now().UTC()
	if err := tickets.Start(c, u, 123, &tm); err != nil {
		t.Error(err)
	}
}

func TestStopTime(t *testing.T) {
	c := &testTimeClient{}
	u := testhelper.NewReturner()

	tm := time.Now().UTC()
	if err := tickets.Stop(c, u, 789, &tm); err != nil {
		t.Error(err)
	}
}

func TestBreakTime(t *testing.T) {
	c := &testTimeClient{}
	u := testhelper.NewReturner()

	tm := time.Now().UTC()
	if err := tickets.Break(c, u, 234, &tm); err != nil {
		t.Error(err)
	}
}

// make sure the json doesn't include time "zero values"
func TestTimeValuesAreNullInJSON(t *testing.T) {
	tm := tickets.Time{}
	tj, err := json.Marshal(tm)
	if err != nil {
		t.Error(err)
	}

	if strings.Contains(string(tj), "ticket_id") {
		t.Errorf("should not include ticket_id: %s", tj)
	}

	if strings.Contains(string(tj), "description") {
		t.Errorf("should not include description: %s", tj)
	}

	if strings.Contains(string(tj), "started_at") {
		t.Errorf("should not include started_at: %s", tj)
	}

	if strings.Contains(string(tj), "broke_at") {
		t.Errorf("should not contain broke_at: %s", tj)
	}

	if strings.Contains(string(tj), "ended_at") {
		t.Errorf("should not contain ended_at: %s", tj)
	}
}

func TestListTimes(t *testing.T) {
	c := &testTimeClient{}
	u := testhelper.NewReturner()

	if err := tickets.ListTimes(c, u); err != nil {
		t.Error(err)
	}
}
