package tickets

import (
	"bufio"
	"bytes"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/display"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//EditTimes opens an editor with the times returned by user-entered
//query parameters and allows the user to edit the times
func EditTimes(c client.TimeClient, u ui.Interactor, tIDs []int64, start time.Time, end time.Time) error {
	q := api.TimeQuery{
		TicketIds:     tIDs,
		StartedAfter:  start.UnixNano(),
		StartedBefore: end.UnixNano(),
	}
	r, err := c.GetTimes(q)
	if err != nil {
		return errors.Wrap(err, "error getting times to edit")
	}

	times := []Time{}
	for _, t := range r {
		times = append(times, aToTime(t))
	}

	if len(times) < 1 {
		u.Displayln("no times found")
		return nil
	}

	b, err := byteifyTimes(times, u.Location())
	if err != nil {
		return errors.Wrap(err, "error converting times to bytes")
	}

	n, err := u.Edit(b)
	if err != nil {
		return errors.Wrap(err, "error editing times")
	}

	newTimes, err := debyteifyTimes(n, u.Location())
	if err != nil {
		return errors.Wrap(err, "error converting bytes to times")
	}

	diff := diffTimes(times, newTimes)
	displayTimeDiff(u, diff)
	if err := u.Wait(); err != nil {
		return errors.Wrap(err, "error getting user input on diff")
	}

	return submitTimeUpdates(c, diff)
}

func submitTimeUpdates(c client.TimeClient, d timeDiff) error {
	//TODO combine all this into one call
	wg := &sync.WaitGroup{}
	ec := make(chan error)

	for _, t := range d.Creates {
		wg.Add(1)

		go func(t Time) {
			if err := c.CreateTime(timeToA(t)); err != nil {
				ec <- errors.Wrapf(err, "failed creating time for ticket %d starting %v and ending %v", t.TicketID, t.StartedAt, t.EndedAt)
			}

			wg.Done()
		}(t)
	}

	for _, t := range d.Updates {
		wg.Add(1)

		go func(t Time) {
			if err := c.UpdateTime(timeToA(t)); err != nil {
				ec <- errors.Wrapf(err, "failed updating time %d", t.ID)
			}
			wg.Done()
		}(t)
	}

	for _, t := range d.Deletes {
		wg.Add(1)

		go func(t Time) {
			if err := c.DeleteTime(timeToA(t)); err != nil {
				ec <- errors.Wrapf(err, "failed deleting time %d", t.ID)
			}
			wg.Done()
		}(t)
	}

	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
		close(ec)
	}()

	errs := &editErrs{}
	for {
		select {
		case err := <-ec:
			if err != nil { // if nil, the channel is closed
				errs.Errors = append(errs.Errors, err)
			}
		case <-done:
			if len(errs.Errors) > 0 {
				return errs
			}

			return nil
		}
	}
}

func byteifyTimes(times []Time, loc *time.Location) ([]byte, error) {
	sort.Slice(times, func(i, j int) bool {
		var (
			t1Start = timeOrZero(times[i].StartedAt)
			t2Start = timeOrZero(times[j].StartedAt)
			t1End   = timeOrZero(times[i].EndedAt)
			t2End   = timeOrZero(times[j].EndedAt)
		)

		if t1Start.Before(t2Start) {
			return true
		}

		if t1Start.Equal(t2Start) {
			if t1End.Before(t2End) {
				return true
			}

			if t1End.Equal(t2End) {
				if times[i].ID < times[j].ID {
					return true
				}
			}
		}

		return false
	})

	b := bytes.NewBuffer([]byte{})
	for _, t := range times {
		//TODO write bytes
		b.WriteString(fmt.Sprintf(
			"%d | %d | %s | %s | %s | %s |\n",
			t.ID,
			t.TicketID,
			t.Description,
			display.EditTimeStr(t.StartedAt, loc),
			display.EditTimeStr(t.BrokeAt, loc),
			display.EditTimeStr(t.EndedAt, loc),
		))
	}

	return b.Bytes(), nil
}

func timeOrZero(t *time.Time) time.Time {
	if t != nil {
		return *t
	}

	return time.Time{}
}

func debyteifyTimes(raw []byte, loc *time.Location) ([]Time, error) {
	newTimes := []Time{}
	le := &LineErrs{}
	scanner := bufio.NewScanner(bytes.NewReader(raw))
	for scanner.Scan() {
		time, err := lineToTime(scanner.Text(), loc)
		if err != nil {
			le.AppendErr(scanner.Text(), err)
			continue
		}

		newTimes = append(newTimes, time)
	}

	if err := scanner.Err(); err != nil {
		return []Time{}, errors.Wrap(err, "error scanning times")
	}

	if err := le.Err(); err != nil {
		return []Time{}, errors.Wrap(err, "error parsing times")
	}

	return newTimes, nil
}

func idStr(ids []int64) string {
	strs := make([]string, 0, len(ids))
	for _, id := range ids {
		strs = append(strs, strconv.FormatInt(id, 10))
	}

	return strings.Join(strs, ",")
}

const (
	idIndex = iota
	ticketIDIndex
	descriptionIndex
	startedAtIndex
	brokeAtIndex
	endedAtIndex
)

func lineToTime(line string, loc *time.Location) (Time, error) {
	// return on the first error. One error could cascade into many, with one cause
	parts := strings.Split(line, "|")

	t := Time{Description: parts[descriptionIndex]}
	var err error

	//if the id is blank, it's a new record
	if id := strings.TrimSpace(parts[idIndex]); id != "" {
		t.ID, err = strconv.ParseInt(id, 10, 64)
		if err != nil {
			return Time{}, errors.Wrap(err, "error parsing ID")
		}
	}

	t.TicketID, err = strconv.ParseInt(strings.TrimSpace(parts[ticketIDIndex]), 10, 64)
	if err != nil {
		return Time{}, errors.Wrap(err, "error parsing ticket ID")
	}

	if t.StartedAt, err = getTime(parts[startedAtIndex], loc); err != nil {
		return Time{}, errors.Wrap(err, "error parsing start time")
	}

	if t.BrokeAt, err = getTime(parts[brokeAtIndex], loc); err != nil {
		return Time{}, errors.Wrap(err, "error parsing break time")
	}

	if t.EndedAt, err = getTime(parts[endedAtIndex], loc); err != nil {
		return Time{}, errors.Wrap(err, "error parsing end time")
	}

	return t, nil
}

func getTime(str string, loc *time.Location) (*time.Time, error) {
	str = strings.TrimSpace(str)
	if str == "" {
		return nil, nil
	}

	t, err := time.ParseInLocation(display.EditFormat, str, loc)
	if err != nil {
		return nil, errors.Wrap(err, "error parsing time")
	}

	t = t.UTC()
	return &t, nil
}

type timeDiff struct {
	Creates []Time
	Updates []Time
	Deletes []Time
}

func diffTimes(ot, nt []Time) timeDiff {
	td := timeDiff{
		Deletes: findTimeDeletes(ot, nt),
	}

OUTER:
	for _, n := range nt {
		for _, o := range ot {
			if n.ID == o.ID {
				if timeUpdated(n, o) {
					td.Updates = append(td.Updates, n)
				}

				continue OUTER
			}
		}
		td.Creates = append(td.Creates, n)
	}

	return td
}

//timeUpdated compares only the fields that a user is allowed to update
func timeUpdated(t1, t2 Time) bool {
	same := compareTimes(t1.StartedAt, t2.StartedAt) &&
		compareTimes(t1.BrokeAt, t2.BrokeAt) &&
		compareTimes(t1.EndedAt, t2.EndedAt)

	return !same
}

//compareTimes compares by truncating off sub-second values, which
//are not available to edit on the edit page
func compareTimes(t1, t2 *time.Time) bool {
	if t1 == nil && t2 == nil {
		return true
	}

	//if either, but not both, are nil
	if t1 == nil || t2 == nil {
		return false
	}

	return t1.Unix() == t2.Unix()
}

func findTimeDeletes(ot, nt []Time) []Time {
	d := []Time{}
OUTER:
	for _, o := range ot {
		for _, n := range nt {
			if o.ID == n.ID {
				continue OUTER
			}
		}
		d = append(d, o)
	}

	return d
}

func displayTimeDiff(u ui.Interactor, td timeDiff) {
	if len(td.Creates)+len(td.Updates)+len(td.Deletes) == 0 {
		u.Displayln("No changes made")
		return
	}

	displayTimeLine(u, td.Creates, "create")
	displayTimeLine(u, td.Updates, "update")
	displayTimeLine(u, td.Deletes, "delete")
}

func displayTimeLine(u ui.Interactor, t []Time, c string) {
	if l := len(t); l > 0 {
		u.Displayf("This operation will %s %d time", c, l)
		if l > 1 {
			u.Displayf("s")
		}
		u.Displayln("")
	}
}
