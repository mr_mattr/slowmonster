package tickets

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//Amount is numerical value tied to a ticket at a specific time
type Amount struct {
	ID          int64     `json:"id,omitempty"`
	TicketID    int64     `json:"ticket_id"`
	Description string    `json:"ticket_description,omitempty"`
	Amount      float64   `json:"amount"`
	AmountedAt  time.Time `json:"amounted_at"`
}

//Add adds a new amount to a ticket
func Add(c client.AmountClient, u ui.Interactor, ticketID int64, a float64, amountedAt time.Time) error {
	am := api.Amount{
		TicketId:   ticketID,
		Amount:     a,
		AmountedAt: amountedAt.UnixNano(),
	}

	if err := c.CreateAmount(am); err != nil {
		return errors.Wrap(err, "API error")
	}

	u.Displayf("%f added to ticket\n", a)

	return nil
}

func aToAmount(a api.Amount) Amount {
	return Amount{
		ID:          a.Id,
		TicketID:    a.TicketId,
		Description: a.Description,
		Amount:      a.Amount,
		AmountedAt:  time.Unix(0, a.AmountedAt),
	}
}

func amountToA(a Amount) api.Amount {
	return api.Amount{
		TicketId:    a.TicketID,
		Description: a.Description,
		Amount:      a.Amount,
		AmountedAt:  a.AmountedAt.UnixNano(),
	}
}
