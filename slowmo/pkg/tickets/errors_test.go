package tickets_test

import (
	"fmt"
	"testing"

	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
)

func TestLineErrsError(t *testing.T) {
	l := tickets.LineErrs{
		LineErrors: []tickets.LineErr{
			tickets.LineErr{
				Line:  "line one",
				Error: fmt.Errorf("error one"),
			},
			tickets.LineErr{
				Line:  "line two",
				Error: fmt.Errorf("error two"),
			},
		},
	}

	expected := `There were 2 errors processing those times

error one
line one

error two
line two

`

	if l.Error() != expected {
		t.Error()
	}
}

func TestLineErrsAppendErr(t *testing.T) {
	l := tickets.LineErrs{
		LineErrors: []tickets.LineErr{
			tickets.LineErr{
				Line:  "line one",
				Error: fmt.Errorf("error one"),
			},
		},
	}

	l.AppendErr("new line", fmt.Errorf("new error"))

	if len(l.LineErrors) != 2 {
		t.Error("wrong number of line errors")
	}

	if l.LineErrors[1].Error.Error() != "new error" {
		t.Error("unexpected error in LineErrors")
	}
}

func TestErrWithoutLineErrors(t *testing.T) {
	if le := &(tickets.LineErrs{}); le.Err() != nil {
		t.Errorf("empty LineErr should return nil, not %s", le.Err().Error())
	}
}
