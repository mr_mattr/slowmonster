package tickets

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//Ticket is an activity that a user can log time or amounts against
type Ticket struct {
	ID          int64      `json:"id"`
	Description string     `json:"description"`
	ClosedAt    *time.Time `json:"closed_at"`
	DaysInWeek  float32    `json:"days_in_week"`
	Priority    int64      `json:"priority"`
}

//Create adds a new ticket to the user's list of tickets
func Create(c client.TicketClient, u ui.Interactor, description string) error {
	t := api.Ticket{
		Description: description,
	}

	if err := c.CreateTicket(t); err != nil {
		return errors.Wrap(err, "API error")
	}

	u.Displayf("Ticket created: %s\n", description)
	return nil
}

//ListTickets displays the tickets assigned to a user
func ListTickets(c client.TicketClient, u ui.Interactor) error {
	r, err := c.GetTickets()
	if err != nil {
		return errors.Wrap(err, "API error")
	}

	tickets := []Ticket{}
	for _, a := range r {
		tickets = append(tickets, aToTicket(a))
	}

	for _, t := range tickets {
		u.Displayf("%d - %s\n", t.ID, t.Description)
	}

	return nil
}

func aToTicket(a api.Ticket) Ticket {
	c := time.Unix(0, a.ClosedAt)
	return Ticket{
		ID:          a.Id,
		Description: a.Description,
		ClosedAt:    &c,
		DaysInWeek:  float32(a.DaysInWeek),
		Priority:    a.Priority,
	}
}
