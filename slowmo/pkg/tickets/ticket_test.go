package tickets_test

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"
	"time"

	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
	"gitlab.com/mr_mattr/slowmonster/slowmo/testhelper"
)

func TestCreateTicket(t *testing.T) {
	s := &TestTicketClient{}
	u := testhelper.NewReturner()

	if err := tickets.Create(s, u, "good ole ticket"); err != nil {
		t.Error(err)
	}
}

func TestCreateBadTicket(t *testing.T) {
	s := &TestTicketClient{
		Error: fmt.Errorf("bad ticket"),
	}
	u := testhelper.NewReturner()

	err := tickets.Create(s, u, "")
	if err == nil {
		t.Error("should have failed to create bad ticket")
	}

	if !strings.Contains(err.Error(), "bad ticket") {
		t.Errorf("bad error message: %s", err)
	}
}

// make sure the json doesn't include time "zero values"
func TestTicketTimeValuesAreNullInJSON(t *testing.T) {
	tk := tickets.Ticket{}
	tj, err := json.Marshal(tk)
	if err != nil {
		t.Error(err)
	}

	if !strings.Contains(string(tj), `"closed_at":null`) {
		t.Errorf("closed at is not null: %s", tj)
	}
}

func TestListTickets(t *testing.T) {
	s := &TestTicketClient{
		Tickets: []api.Ticket{{
			Id:          123,
			Description: "pretty good ticket",
			ClosedAt:    time.Now().UnixNano(),
			Priority:    456,
		}},
	}
	u := testhelper.NewReturner()

	if err := tickets.ListTickets(s, u); err != nil {
		t.Error(err)
	}
}

type TestTicketClient struct {
	Tickets []api.Ticket
	Error   error
}

func (c *TestTicketClient) GetTickets() ([]api.Ticket, error) {
	return c.Tickets, c.Error
}

func (c *TestTicketClient) CreateTicket(t api.Ticket) error {
	return c.Error
}

func (c *TestTicketClient) UpdateTicket(t api.Ticket) error {
	return c.Error
}

func (c *TestTicketClient) DeleteTicket(t api.Ticket) error {
	return c.Error
}
