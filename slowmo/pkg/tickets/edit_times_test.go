package tickets_test

import (
	"strings"
	"testing"
	"time"

	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
	"gitlab.com/mr_mattr/slowmonster/slowmo/testhelper"
)

const (
	timeJSON = `{"data":
	[{"id":123,
	"ticket_id":456,
	"ticket_description": "good ticket",
	"started_at":"2017-01-01T00:01:01.000000Z",
	"broke_at":"2017-01-01T00:01:02.000000Z",
	"ended_at":"2017-01-01T00:01:03.000000Z"}]}`

	startTime = 1483228861000000000 //2017-01-01 00:01:01

	timeStr = "123 | 456 | good ticket | 2017-01-01 00:01:01 | 2017-01-01 00:01:02 | 2017-01-01 00:01:03 |\n"
)

func TestEditTimesNoChange(t *testing.T) {
	s := &testTimeClient{
		Times: []api.Time{{
			Id:          123,
			TicketId:    456,
			Description: "good ticket",
			StartedAt:   startTime,
			BrokeAt:     startTime + 1000000000,
			EndedAt:     startTime + 2000000000,
		}},
	}
	r := testhelper.NewReturner()
	r.SetEdits([]byte(timeStr), []byte(timeStr))

	if err := tickets.EditTimes(s, r, []int64{}, time.Time{}, time.Time{}); err != nil {
		t.Error(err)
	}
}

func TestEditTimesCreate(t *testing.T) {
	newTime := " | 012 | second ticket | 2017-01-02 00:01:01 | 2017-01-02 00:01:02 | 2017-01-02 00:01:03 |\n"

	s := &testTimeClient{
		Times: []api.Time{{
			Id:          123,
			TicketId:    456,
			Description: "good ticket",
			StartedAt:   startTime,
			BrokeAt:     startTime + 1000000000,
			EndedAt:     startTime + 2000000000,
		}},
	}
	r := testhelper.NewReturner()
	r.SetEdits([]byte(timeStr), []byte(timeStr+newTime))

	ogCreateCount := s.createCount
	if err := tickets.EditTimes(s, r, []int64{}, time.Time{}, time.Time{}); err != nil {
		t.Fatal(err)
	}

	if s.createCount != ogCreateCount+1 {
		t.Error("the create endpoint was not called")
	}
}

func TestEditTimesUpdate(t *testing.T) {
	updatedTime := "123 | 456 | good ticket | 2017-01-02 00:01:01 | 2017-01-02 00:01:02 | 2017-01-02 00:01:03 |\n"

	s := &testTimeClient{
		Times: []api.Time{{
			Id:          123,
			TicketId:    456,
			Description: "good ticket",
			StartedAt:   startTime,
			BrokeAt:     startTime + 1000000000,
			EndedAt:     startTime + 2000000000,
		}},
	}
	r := testhelper.NewReturner()
	r.SetEdits([]byte(timeStr), []byte(updatedTime))

	ogUpdateCount := s.updateCount
	if err := tickets.EditTimes(s, r, []int64{}, time.Time{}, time.Time{}); err != nil {
		t.Fatal(err)
	}

	if s.updateCount != ogUpdateCount+1 {
		t.Error("the update endpoint was not called")
	}
}

func TestEditTimesDelete(t *testing.T) {
	s := &testTimeClient{
		Times: []api.Time{{
			Id:          123,
			TicketId:    456,
			Description: "good ticket",
			StartedAt:   startTime,
			BrokeAt:     startTime + 1000000000,
			EndedAt:     startTime + 2000000000,
		}},
	}
	r := testhelper.NewReturner()
	r.SetEdits([]byte(timeStr), []byte(""))

	ogDeleteCount := s.deleteCount
	if err := tickets.EditTimes(s, r, []int64{}, time.Time{}, time.Time{}); err != nil {
		t.Fatal(err)
	}

	if s.deleteCount != ogDeleteCount+1 {
		t.Error("the delete endpoint was not called")
	}
}

func TestLineErrs(t *testing.T) {
	updatedTime := "abc | 456 | good ticket | 2017-01-02 00:01:01 | 2017-01-02 00:01:02 | 2017-01-02 00:01:03 |\n"

	s := &testTimeClient{
		Times: []api.Time{{
			Id:          123,
			TicketId:    456,
			Description: "good ticket",
			StartedAt:   startTime,
			BrokeAt:     startTime + 1000000000,
			EndedAt:     startTime + 2000000000,
		}},
	}
	r := testhelper.NewReturner()
	r.SetEdits([]byte(timeStr), []byte(updatedTime))

	ogUpdateCount := s.updateCount
	err := tickets.EditTimes(s, r, []int64{}, time.Time{}, time.Time{})
	if err == nil {
		t.Fatal("EditTimes should have returned an error")
	}

	if !strings.Contains(err.Error(), "error parsing ID") {
		t.Errorf("incorrect error returned: %s", err)
	}

	if s.updateCount != ogUpdateCount {
		t.Error("the update endpoint was called when it shouldn't have been")
	}
}

type testTimeClient struct {
	Times       []api.Time
	Error       error
	createCount int
	updateCount int
	deleteCount int
}

func (c *testTimeClient) GetTimes(api.TimeQuery) ([]api.Time, error) {
	return c.Times, c.Error
}

func (c *testTimeClient) CreateTime(api.Time) error {
	c.createCount++
	return c.Error
}

func (c *testTimeClient) UpdateTime(api.Time) error {
	c.updateCount++
	return c.Error
}

func (c *testTimeClient) DeleteTime(api.Time) error {
	c.deleteCount++
	return c.Error
}
