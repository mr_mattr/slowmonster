package ui

import "time"

//Interactor is the interface that allows input/output from/to the user
type Interactor interface {
	Wait() error
	Edit([]byte) ([]byte, error)
	Display(...interface{})
	Displayf(string, ...interface{})
	Displayln(...interface{})
	Read() (string, error)
	ReadPassword() (string, error)
	SetLocation(*time.Location)
	Location() *time.Location
}
