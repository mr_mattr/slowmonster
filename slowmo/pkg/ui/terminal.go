package ui

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"syscall"
	"time"

	"github.com/pkg/errors"

	"golang.org/x/crypto/ssh/terminal"
)

//Terminal allows command-line interaction with the user
type Terminal struct {
	editor string
	loc    *time.Location
}

//NewTerminal returns a new terminal instance with the provided editor
func NewTerminal(e string, l *time.Location) *Terminal {
	return &Terminal{
		editor: e,
		loc:    l,
	}
}

//SetLocation sets the client's local location
func (t *Terminal) SetLocation(l *time.Location) {
	t.loc = l
}

//Location returns the user's location (to determine timezone)
func (t *Terminal) Location() *time.Location {
	return t.loc
}

//Wait displays a message and waits for the user to press enter
func (t *Terminal) Wait() error {
	fmt.Println("Press enter to continue, Ctrl-c to exit")
	reader := bufio.NewReader(os.Stdin)
	_, err := reader.ReadString('\n')
	return err
}

//Edit opens contents in an editor and allows the user to edit them
func (t *Terminal) Edit(og []byte) ([]byte, error) {
	fileName, err := setupFile(og)
	if err != nil {
		return []byte{}, errors.Wrap(err, "error setting up a file to edit")
	}

	if err := editFile(fileName, t.editor); err != nil {
		return []byte{}, errors.Wrap(err, "error editing file")
	}

	return ioutil.ReadFile(fileName)
}

func setupFile(contents []byte) (string, error) {
	f, err := ioutil.TempFile("", "slowmo-times")
	if err != nil {
		return "", errors.Wrap(err, "error opening temporary file to edit")
	}

	if _, err := f.Write(contents); err != nil {
		return "", errors.Wrap(err, "error writing contents to temporary file")
	}

	return f.Name(), nil
}

func editFile(fileName string, editor string) error {
	cmd := exec.Command(editor, fileName)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		return errors.Wrap(err, "error starting editor")
	}

	if err := cmd.Wait(); err != nil {
		return errors.Wrap(err, "error waiting for editor")
	}

	return nil
}

//Display displays a string
func (t *Terminal) Display(a ...interface{}) {
	fmt.Print(a...)
}

//Displayf displays an interpolated string
func (t *Terminal) Displayf(c string, a ...interface{}) {
	fmt.Printf(c, a...)
}

//Displayln displays a string followed by a carriage return
func (t *Terminal) Displayln(a ...interface{}) {
	fmt.Println(a...)
}

//Read reads from stdin until a carriage return, then trims any whitespace
func (t *Terminal) Read() (string, error) {
	reader := bufio.NewReader(os.Stdin)

	e, err := reader.ReadString('\n')
	if err != nil {
		return "", errors.Wrap(err, "Username broke a thing")
	}

	return strings.TrimSpace(e), nil
}

//ReadPassword reads from stdin, not displaying the results to the terminal, until a carriage return, then trims any whitespace
func (t *Terminal) ReadPassword() (string, error) {
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	fmt.Println("") // otherwise later prints start on this line
	if err != nil {
		return "", errors.Wrap(err, "Password broke a thing")
	}

	return strings.TrimSpace(string(bytePassword)), nil
}
