package client

import (
	"context"
	"fmt"
	"io"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
)

//TimeClient is the interfacee to the time API
type TimeClient interface {
	GetTimes(api.TimeQuery) ([]api.Time, error)
	CreateTime(api.Time) error
	UpdateTime(api.Time) error
	DeleteTime(api.Time) error
}

//GetTimes calls the API to get times according
//to query results
func (s *Session) GetTimes(q api.TimeQuery) ([]api.Time, error) {
	c := api.NewTimeServClient(s.Conn)

	stream, err := c.GetTimes(context.Background(), &q)
	if err != nil {
		return []api.Time{}, err
	}

	times := []api.Time{}
	for {
		t, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return times, errors.Wrap(err, "there was an error receiving times from the API")
		}

		times = append(times, *t)
	}

	return times, nil
}

//CreateTime calls the API to create a time
func (s *Session) CreateTime(t api.Time) error {
	c := api.NewTimeServClient(s.Conn)

	_, err := c.CreateTime(context.Background(), &t)
	if err != nil {
		return err
	}

	return nil
}

//UpdateTime calls the API to update a time
func (s *Session) UpdateTime(t api.Time) error {
	c := api.NewTimeServClient(s.Conn)

	newT, err := c.UpdateTime(context.Background(), &t)
	if err != nil {
		return err
	}

	fmt.Printf("###### updated time: %#v\n", newT)
	return nil
}

//DeleteTime calls the API to delete a time
func (s *Session) DeleteTime(t api.Time) error {
	c := api.NewTimeServClient(s.Conn)

	if _, err := c.DeleteTime(context.Background(), &t); err != nil {
		return err
	}

	return nil
}
