package client

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
)

//UserClient is the interface to the user api
type UserClient interface {
	Signup(u *api.User) (*api.User, error)
}

//Signup calls the signup method on the API
func (s *Session) Signup(u *api.User) (*api.User, error) {
	c := api.NewUserServClient(s.Conn)

	resp, err := c.CreateUser(context.Background(), u)
	if err != nil {
		return &api.User{}, errors.Wrap(err, "failed creating user")
	}

	return resp, nil
}
