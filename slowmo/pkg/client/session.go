package client

import (
	"context"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

//Session represents a session with the API endpoint
type Session struct {
	Conn      *grpc.ClientConn
	TokenFile string
}

//NewSession opens a connection to the API and returns a populated session
func NewSession(baseURL string, port int, tokenFile string, insecure bool) (*Session, error) {
	s := &Session{
		TokenFile: tokenFile,
	}

	opts := []grpc.DialOption{
		grpc.WithUnaryInterceptor(s.UnaryInterceptor),
		grpc.WithStreamInterceptor(s.StreamInterceptor),
	}

	if insecure {
		opts = append(opts, grpc.WithInsecure())
	} else {
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{})))
	}

	var err error
	s.Conn, err = grpc.Dial(fmt.Sprintf("%s:%d", baseURL, port), opts...)
	if err != nil {
		return &Session{}, errors.Wrap(err, "failed to dial server")
	}

	return s, nil
}

//CloseConn closes the client connection to the API
func (s *Session) CloseConn() error {
	if s == nil || s.Conn == nil {
		return nil
	}

	if err := s.Conn.Close(); err != nil {
		return errors.Wrap(err, "error closing API connection")
	}

	return nil
}

//SetToken sets the auth token in a context
func (s *Session) SetToken(ctx context.Context) (context.Context, error) {
	token, err := ioutil.ReadFile(s.TokenFile)
	if err != nil {
		// return original context in case of error
		return ctx, errors.Wrap(err, "failed to read token file")
	}

	md := metadata.Pairs("token", string(token))

	return metadata.NewOutgoingContext(ctx, md), nil
}

//UnaryInterceptor adds the authentication token to the unary request
func (s *Session) UnaryInterceptor(
	ctx context.Context,
	method string,
	req interface{},
	reply interface{},
	cc *grpc.ClientConn,
	invoker grpc.UnaryInvoker,
	opts ...grpc.CallOption,
) error {
	t, err := s.SetToken(ctx)
	if err != nil {
		log.Printf("there was an error setting the auth token (no token set in request): %s", err)
	}
	return invoker(t, method, req, reply, cc, opts...)
}

//StreamInterceptor adds the authentication token to the streaming request
func (s *Session) StreamInterceptor(
	ctx context.Context,
	desc *grpc.StreamDesc,
	cc *grpc.ClientConn,
	method string,
	streamer grpc.Streamer,
	opts ...grpc.CallOption,
) (grpc.ClientStream, error) {
	t, err := s.SetToken(ctx)
	if err != nil {
		//TODO don't show an error in certain circumstances, for example:
		//when they haven't logged in yet and there's no tokenfile
		log.Printf("there was an error setting the auth token (no token set in request): %s", err)
	}
	return streamer(t, desc, cc, method, opts...)
}
