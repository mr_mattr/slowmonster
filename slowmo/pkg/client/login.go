package client

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
)

//SessionClient is the interface to the session api
type SessionClient interface {
	Login(l *api.Login) (*api.Session, error)
	GetTokenFile() string
}

//Login calls the login method on the API
func (s *Session) Login(l *api.Login) (*api.Session, error) {
	c := api.NewSessionServClient(s.Conn)

	resp, err := c.Create(context.Background(), l)
	if err != nil {
		return &api.Session{}, errors.Wrap(err, "failed logging in to the server")
	}

	return resp, nil
}

//GetTokenFile returns the token filename
func (s *Session) GetTokenFile() string {
	return s.TokenFile
}
