package client

import (
	"context"
	"fmt"
	"io"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
)

//TicketClient is the interface to the tickets api
type TicketClient interface {
	GetTickets() ([]api.Ticket, error)
	CreateTicket(api.Ticket) error
	UpdateTicket(api.Ticket) error
	DeleteTicket(api.Ticket) error
}

//GetTickets calls the API to get tickets for the current user
func (s *Session) GetTickets() ([]api.Ticket, error) {
	c := api.NewTicketServClient(s.Conn)

	stream, err := c.GetTickets(context.Background(), &api.TicketQuery{})
	if err != nil {
		return []api.Ticket{}, errors.Wrap(err, "there was an error connecting to the tickets API")
	}

	tickets := []api.Ticket{}
	for {
		t, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return tickets, errors.Wrap(err, "there was an error receiving tickets from the API")
		}

		tickets = append(tickets, *t)
	}

	return tickets, nil
}

//CreateTicket calls the API to create an amount
func (s *Session) CreateTicket(t api.Ticket) error {
	c := api.NewTicketServClient(s.Conn)

	ticket, err := c.CreateTicket(context.Background(), &t)
	if err != nil {
		return err
	}

	fmt.Printf("###### created ticket: %#v\n", ticket)
	return nil
}

//UpdateTicket calls the API to update an amount
func (s *Session) UpdateTicket(api.Ticket) error {
	//TODO
	return nil
}

//DeleteTicket calls the API to delete an amount
func (s *Session) DeleteTicket(api.Ticket) error {
	//TODO
	return nil
}
