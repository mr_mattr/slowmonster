package client

import (
	"context"

	"gitlab.com/mr_mattr/slowmonster/api"
)

//AmountClient is the interface to the amounts API
type AmountClient interface {
	GetAmounts(api.AmountQuery) ([]api.Amount, error)
	CreateAmount(api.Amount) error
	UpdateAmount(api.Amount) error
	DeleteAmount(api.Amount) error
}

//GetAmounts calls the API to get amounts according
//to query params
func (s *Session) GetAmounts(api.AmountQuery) ([]api.Amount, error) {
	//TODO
	return []api.Amount{}, nil
}

//CreateAmount calls the API to create an amount
func (s *Session) CreateAmount(a api.Amount) error {
	c := api.NewAmountServClient(s.Conn)

	_, err := c.CreateAmount(context.Background(), &a)
	if err != nil {
		return err
	}

	return nil
}

//UpdateAmount calls the API to update an amount
func (s *Session) UpdateAmount(api.Amount) error {
	//TODO
	return nil
}

//DeleteAmount calls the API to delete an amount
func (s *Session) DeleteAmount(api.Amount) error {
	//TODO
	return nil
}
