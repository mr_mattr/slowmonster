package client

import "gitlab.com/mr_mattr/slowmonster/api"

//ReportClient is the interface to the reports API
type ReportClient interface {
	TicketTotals(api.TotalsQuery) ([]api.TotalReport, error)
}

//TicketTotals calls the API and returns a slice of tickets
func (s *Session) TicketTotals(q api.TotalsQuery) ([]api.TotalReport, error) {
	//TODO
	return []api.TotalReport{}, nil
}
