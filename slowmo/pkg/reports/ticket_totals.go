package reports

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/display"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//TicketReport represents represents ticket data given some parameters
type TicketReport struct {
	TicketID    int64   `json:"id"`
	Description string  `json:"description"`
	Total       float64 `json:"total"`
	Totals      []Total `json:"totals"`
}

//Total is the total time/amount between two times
type Total struct {
	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`
	Total     float64   `json:"total"`
}

//TicketTotals retrieves and displays the report information
func TicketTotals(c client.ReportClient, u ui.Interactor, tIDs []int64, start time.Time, end time.Time, reportType string) error {
	q := api.TotalsQuery{
		TicketIds: tIDs,
		Start:     start.UnixNano(),
		End:       end.UnixNano(),
	}

	rs, err := c.TicketTotals(q)
	if err != nil {
		return errors.Wrap(err, "error getting report from the API")
	}

	var reports []TicketReport
	for _, r := range rs {
		reports = append(reports, aToTicketReport(r))
	}

	for _, t := range reports {
		u.Displayf("%d - %s\n", t.TicketID, t.Description)

		if t.Total != 0.0 {
			u.Displayf("Total: %f\n", t.Total)
		}

		for _, total := range t.Totals {
			u.Displayf("%s - %s: %f\n", display.TimeStr(&total.StartTime, u.Location()), display.TimeStr(&total.EndTime, u.Location()), total.Total)
		}

		u.Displayln("#################")
	}

	return nil
}

func aToTicketReport(a api.TotalReport) TicketReport {
	r := TicketReport{
		TicketID:    a.TicketId,
		Description: a.Description,
		Total:       a.Total,
	}

	for _, t := range a.Totals {
		r.Totals = append(r.Totals, Total{
			StartTime: time.Unix(0, t.StartTime),
			EndTime:   time.Unix(0, t.EndTime),
			Total:     t.Total,
		})
	}

	return r
}
