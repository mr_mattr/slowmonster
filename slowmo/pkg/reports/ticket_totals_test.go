package reports_test

import (
	"testing"
	"time"

	"gitlab.com/mr_mattr/slowmonster/api"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/reports"
	"gitlab.com/mr_mattr/slowmonster/slowmo/testhelper"
)

func TestReport(t *testing.T) {
	c := &TestReportClient{
		Reports: []api.TotalReport{},
	}

	u := testhelper.NewReturner()

	if err := reports.TicketTotals(c, u, []int64{1, 2}, time.Now().UTC(), time.Now().UTC(), "total"); err != nil {
		t.Error(err)
	}
}

type TestReportClient struct {
	Reports []api.TotalReport
	Error   error
}

func (c *TestReportClient) TicketTotals(q api.TotalsQuery) ([]api.TotalReport, error) {
	return c.Reports, c.Error
}
