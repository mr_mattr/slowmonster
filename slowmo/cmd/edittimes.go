// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
)

// editTimesCmd represents the edit-times command
var editTimesCmd = &cobra.Command{
	Use:   "edit-times",
	Short: "edits times",
	Long:  `edits times`,
	Run: func(cmd *cobra.Command, args []string) {
		ids, err := parseIDs(ticketIDs)
		if err != nil {
			u.Displayln(err)
			os.Exit(1)
		}

		st, err := parseTime(startTime, u.Location())
		if err != nil {
			u.Displayln(err)
			os.Exit(1)
		}

		et, err := parseTime(endTime, u.Location())
		if err != nil {
			u.Displayln(err)
			os.Exit(1)
		}

		if err := tickets.EditTimes(apiClient, u, ids, st, et); err != nil {
			u.Displayln(err)
			os.Exit(1)
		}
	},
}

func init() {
	RootCmd.AddCommand(editTimesCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// currentCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// currentCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	editTimesCmd.Flags().StringVar(&ticketIDs, "ticket-ids", "", "comma-separated IDs of the tickets to include")
	editTimesCmd.Flags().StringVar(&startTime, "start", "", "start time")
	editTimesCmd.Flags().StringVar(&endTime, "end", "", "end time")
}

func parseIDs(tIDs string) ([]int64, error) {
	idStrs := strings.Split(tIDs, ",")
	ids := make([]int64, len(idStrs))
	for idx, idStr := range idStrs {
		i, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			return ids, errors.Wrap(err, fmt.Sprintf("ticket ID is not an integer: %s", idStr))
		}

		ids[idx] = i
	}

	return ids, nil
}

func parseTime(t string, l *time.Location) (time.Time, error) {
	if t == "" {
		return time.Time{}, nil
	}
	return time.ParseInLocation("2006-01-02T15:04:05", t, l)
}
