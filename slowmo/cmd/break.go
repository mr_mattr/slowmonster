// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
)

// breakCmd represents the break command
var breakCmd = &cobra.Command{
	Use:   "break",
	Short: "take a break",
	Long:  `take a break`,
	Run: func(cmd *cobra.Command, args []string) {
		breakTime := time.Now().UTC()
		if err := tickets.Break(apiClient, u, timeID, &breakTime); err != nil {
			u.Displayln(err)
			os.Exit(1)
		}
	},
}

func init() {
	RootCmd.AddCommand(breakCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// breakCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// breakCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	breakCmd.Flags().Int64Var(&timeID, "time-id", 0, "the ID of the time to break")
}
