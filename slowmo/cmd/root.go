// Copyright © 2017 Matt Rose <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"
	"time"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/client"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/ui"
)

//TODO make this configurable
const editor = "vim"

var (
	cfgFile   string
	apiClient *client.Session
	u         ui.Interactor
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "slowmo",
	Short: "A tracker of times and things",
	Long: `A tracker of the following:
  * times
  * things`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	defer func() {
		if err := apiClient.CloseConn(); err != nil {
			u.Displayln(err)
			os.Exit(1)
		}
	}()

	if err := RootCmd.Execute(); err != nil {
		//TODO would it make sense to initialize u earlier?
		if u == nil {
			//u has not been initialized
			fmt.Println(err)
		} else {
			u.Displayln(err)
		}
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.slowmo.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	u = ui.NewTerminal(editor, time.Local)

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			u.Displayln(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".slowmo" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath(".")
		viper.SetConfigName(".slowmo")

		viper.SetEnvPrefix("slowmocli")
		viper.AutomaticEnv() // read in environment variables that match
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		u.Displayln("Using config file:", viper.ConfigFileUsed())
	}

	if err := setClient(); err != nil {
		u.Displayf("Error setting up API connection: %s\n", err)
		os.Exit(1)
	}

	locStr := viper.GetString("local.location")
	location, err := getLocation(locStr)
	if err != nil {
		u.Displayf("bad time zone location given: %s\n", locStr)
		os.Exit(1)
	}

	u.SetLocation(location)
}

func getLocation(locStr string) (*time.Location, error) {
	if locStr == "" {
		return time.Local, nil
	}

	return time.LoadLocation(locStr)
}

func setClient() error {
	baseURL := viper.GetString("api_url")
	if baseURL == "" {
		return errors.New("API URL is required")
	}

	port := viper.GetInt("api_port")
	if port == 0 {
		return errors.New("API Port is required")
	}

	tokenFile := viper.GetString("api_tokenfile")
	if tokenFile == "" {
		return errors.New("Token file is required")
	}

	insecure := viper.GetBool("api_insecure")

	var err error
	if apiClient, err = client.NewSession(baseURL, port, tokenFile, insecure); err != nil {
		return errors.Wrapf(err, "error setting up API connection")
	}

	return nil
}
