// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/reports"
)

var ticketIDs, startTime, endTime, reportType string

// reportCmd represents the report command
var reportCmd = &cobra.Command{
	Use:   "report",
	Short: "reports times and amounts logged against ticket(s)",
	Long:  `reports times and amount logged against ticket(s)`,
	Run: func(cmd *cobra.Command, args []string) {
		ids, err := parseIDs(ticketIDs)
		if err != nil {
			u.Displayln(err)
			os.Exit(1)
		}

		st, err := parseTime(startTime, u.Location())
		if err != nil {
			u.Displayln(err, u.Location())
			os.Exit(1)
		}

		et, err := parseTime(endTime, u.Location())
		if err != nil {
			u.Displayln(err)
			os.Exit(1)
		}

		if err := reports.TicketTotals(apiClient, u, ids, st, et, reportType); err != nil {
			u.Displayln(err)
			os.Exit(1)
		}
	},
}

func init() {
	RootCmd.AddCommand(reportCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// reportCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// reportCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	reportCmd.Flags().StringVar(&ticketIDs, "ticket-ids", "", "comma-separated IDs of the tickets to include in the report")
	reportCmd.Flags().StringVar(&startTime, "start", "", "start time")
	reportCmd.Flags().StringVar(&endTime, "end", "", "end time")
	reportCmd.Flags().StringVar(&reportType, "type", "total", "report type (total,daily,weekly,monthly)")
}
