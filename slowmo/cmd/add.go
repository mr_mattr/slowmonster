// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/mr_mattr/slowmonster/slowmo/pkg/tickets"
)

var amount float64

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add",
	Short: "add an amount to a ticket",
	Long:  `add an amount to a ticket`,
	Run: func(cmd *cobra.Command, args []string) {
		amountedAt := time.Now().UTC()

		if err := tickets.Add(apiClient, u, ticketID, amount, amountedAt); err != nil {
			u.Displayln(err)
			os.Exit(1)
		}
	},
}

func init() {
	RootCmd.AddCommand(addCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// addCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// addCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	addCmd.Flags().Int64Var(&ticketID, "ticket-id", 0, "the ID of the ticket to add to")
	addCmd.Flags().Float64Var(&amount, "amount", 0, "the amount to add")
}
