FROM debian:buster

RUN apt-get update
RUN apt-get install -y git curl build-essential

RUN curl -SL https://golang.org/dl/go1.15.5.linux-amd64.tar.gz | tar -xzC /usr/local

COPY . /root/code
RUN echo "export PATH=$PATH:/usr/local/go/bin:/root/go/bin" >> ~/.bashrc

WORKDIR /root/code/cli
RUN /usr/local/go/bin/go install

WORKDIR /root/code/server/migrate
RUN /usr/local/go/bin/go install

WORKDIR /root/code/server
RUN /usr/local/go/bin/go install

RUN /usr/local/go/bin/go get -u github.com/spf13/cobra/cobra

CMD /root/go/bin/server
